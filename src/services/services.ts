import {Livre} from "../models/Livres";
import {Cd} from "../models/Cd";

export class AllServices{
  CdList: Cd[] = [
    {
      name: 'Harry Potter',
      realisateur: 'David Yates',
      isOn: false
    },
    {
      name: 'Seigneur Des Anneaux',
      realisateur: 'Peter Jackson',
      isOn: true
    },
    {
      name: 'Pirate Des Caraibe',
      realisateur: 'Gore Verbinski',
      isOn: true
    }
  ];

  LivresList: Livre[] = [
    {
      name: 'Drangon Ball Z',
      auteur: 'Akira Toriyama',
      isOn: true

    },
    {
      name: 'Naruto',
      auteur:'Masashi Kishimoto',
      isOn: false

    },
    {
      name: 'One Piece',
      auteur: 'Eiichirō Oda',
      isOn: true
    }
  ];
}
