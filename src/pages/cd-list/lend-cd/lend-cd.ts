import {Component, OnInit} from "@angular/core";
import {NavParams, ViewController} from "ionic-angular";
import {Cd} from "../../../models/Cd";
import {AllServices} from "../../../services/services";

@Component({
  selector: 'page-lend-cd',
  templateUrl: 'lend-cd.html'
})

export class LendCdPage implements OnInit{
  index: number;

  cd: Cd;

  constructor(public navParams: NavParams, private viewCtrl: ViewController,
              private allServices: AllServices) {
  }

  ngOnInit(){
    this.index = this.navParams.get('index');
    this.cd = this.allServices.CdList[this.index];
  }

  dismissModal(){
    this.viewCtrl.dismiss()
  }

  onToggleCd(){
    this.cd.isOn = !this.cd.isOn;
  }

}
