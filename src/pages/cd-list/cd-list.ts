import {Component} from "@angular/core";
import {MenuController, ModalController} from "ionic-angular";
import {LendCdPage} from "./lend-cd/lend-cd";
import {Cd} from "../../models/Cd";
import {AllServices} from "../../services/services";

@Component({
  selector: 'page-cd-list',
  templateUrl: 'cd-list.html'
})
export class CdListPage {

  cdList: Cd[];

  constructor(private modalCtrl: ModalController,
              private allService: AllServices,
              private menuCtrl: MenuController) {

  }

  ionViewWillEnter(){
    this.cdList = this.allService.CdList.slice();

  }

  onLoadCd(index: number) {
    let modal = this.modalCtrl.create(LendCdPage, {index: index});
    modal.present();
  }

  onToggleMenu(){
    this.menuCtrl.open();
  }

}
