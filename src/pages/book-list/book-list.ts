import { Component } from '@angular/core';
import {MenuController, ModalController} from "ionic-angular";
import {LendBookPage} from "./lend-book/lend-book";
import {Livre} from "../../models/Livres";
import {AllServices} from "../../services/services";


@Component({
  selector: 'page-book-list',
  templateUrl: 'book-list.html'
})
export class BookListPage {

  livresList: Livre[];

  constructor(private modalCtrl: ModalController,
              private allServices: AllServices,
              private menuCtrl: MenuController){

  }

  ionViewWillEnter(){
    this.livresList = this.allServices.LivresList.slice();
  }

  onLoadLivre(index: number) {
  let modal = this.modalCtrl.create(LendBookPage, {index: index});
  modal.present();
  }

  onToggleMenu(){
    this.menuCtrl.open();
  }

}
