import {Component, OnInit} from "@angular/core";
import {NavParams, ViewController} from "ionic-angular";
import {Livre} from "../../../models/Livres";
import {AllServices} from "../../../services/services";

@Component({
  selector: 'page-lend-book',
  templateUrl: 'lend-book.html'
})

export class LendBookPage implements OnInit{
  index: number;
  livre:Livre;

  constructor(public navParams: NavParams, private viewCtrl : ViewController,
              private allServices: AllServices) {
  }

  ngOnInit(){
    this.index = this.navParams.get('index');
    this.livre = this.allServices.LivresList[this.index];
  }
  dismissModal(){
    this.viewCtrl.dismiss();
  }

  onToggleLivre(){
    this.livre.isOn = !this.livre.isOn;
  }

}
