export class Cd {
  realisateur: string;
  isOn: boolean;

  constructor(public name: string){
    this.isOn = false
  }
}
